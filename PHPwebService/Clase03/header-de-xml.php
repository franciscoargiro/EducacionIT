<?php

$docLibreria = new SimpleXMLElement('<libreria></libreria>');

$libro1 = $docLibreria->addChild('libro');
$libro1->addAttribute('isbn', '9789875662698');
$libro1->addChild('titulo', 'Misery');
$libro1->addChild('autor', 'Stephen King');
$libro1->addChild('editorial', 'Debolsillo');

$libro2 = $docLibreria->addChild('libro');
$libro2->addAttribute('isbn', '9788498381498');

$tituloElPrincipito = $libro2->addChild('titulo', 'El Principito');
$tituloElPrincipito->addAttribute('nombre-original', 'Le petit prince');

$libro2->addChild('autor', 'Antoine de Saint - Exupery');
$libro2->addChild('editorial', 'Salamandra');

header('Content-Type: text/xml; charset=UTF-8');

echo $docLibreria->asXML();