<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>RSS 1</title>
	

	<style>
		body {
			font-family: Verdana, Arial;
			font-size: 14px;
			width: 500px;
			margin: auto;			
		}

		.noticias {
			margin-top: 10px;
			background-color: #C1C0C0;
			padding: 5px;
			margin: auto;
		}

	</style>	

</head>
<body>
	
	<h1>Noticias de El País</h1>
	
	<?php 

		$url = 'http://ep00.epimg.net/rss/elpais/portada_america.xml';

		$doc = new SimpleXMLElement($url, null, true);
		$noticias = $doc->channel->item;

		foreach ($noticias as $key => $value) {
			?>

			<div class="noticias">
				<?php echo $value->description; ?>
			</div>

			<?php

		}; // endforeach


	 ?>
	
</body>
</html>