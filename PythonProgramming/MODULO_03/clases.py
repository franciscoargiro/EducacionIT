class MiClase:
    def __init__(self):
        print("hola Mundo")
        self.a= "Adíos"
        self.__privado = "obtener esto con Set"

    def __suma(self,a,b):
        return a+b

    def imprimir_suma(self,a,b):
        print(self.__suma(a,b))

    def getprivado(self):
        return self.__privado    


instancia01 = MiClase()

print(instancia01.a)

# print(instancia01.__suma(2,5))   # No funciona porque al tener los __ es un metodo privado de la clase

instancia01.imprimir_suma(2,8)

print(instancia01.getprivado())


# Herencia

class MiOtraClase(MiClase):
    pass


instancia02 = MiOtraClase()

instancia02.imprimir_suma(4,10)
