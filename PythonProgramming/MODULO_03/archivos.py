"""
La librería estándar de Python provee varias
funciones para trabajar con el sistema de archivos:
listar, copiar, mover, eliminar, renombrar, etc.
Están desperdigadas en dos módulos: os y shutil.
Dentro del módulo os nos interesan estas funciones:
● os.listdir()
● os.getcwd()
● os.mkdir()
● os.path.exists()
● os.rename()
● os.remove() # borra archivos
● os.rmdir() # borra carpetas
● os.system()
● os.name
"""

import os

os.getcwd()

# lista_del_directorio = os.listdir("c:/REPOSITORIOS/EDUCACION_IT/PythonProgramming")
lista_del_directorio = os.listdir(os.getcwd())

for directorios in lista_del_directorio:
    print(directorios) 

print(os.path.exists("c:/REPOSITORIOS/EDUCACION_IT/PythonProgramming")) # existe -> True
print(os.path.exists("c:/REPOSITORIOS/EDUCACION_IT/inventado")) # inventada arroja un false

# os.system("cmd")
# os.system("msconfig")  

"""
Por otro lado, dentro del módulo shutil nos interesan
las siguientes:
● shutil.copy()
● shutil.move()
● shutil.rmtree() # BORRAMOS CARPETAS QUE NO ESTEN VACÍAS PARA SIEMPRE!!
"""




