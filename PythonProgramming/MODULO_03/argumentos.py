import sys
# Usamos la propiedad de «slicing» para ignorar el primer
# argumento, i. e. el nombre del programa.
nombres = sys.argv[1:]
# Chequeamos que la lista no esté vacía.
if nombres:
 for nombre in nombres:
     print(f"¡Hola, {nombre}!")
else:
    print("No has indicado ningún nombre.")
    