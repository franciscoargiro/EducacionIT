import os 
import sys

# 1. Levantar los elementos pasados por argumentos
ruta = r"C:/Users/EducaciónIT/Desktop/EducaciónIT/Cursos/Python Programming/Módulo 5/Ejercicios/Integrador 1"

try:
    argumentos = sys.argv
    extension = argumentos[1]
except Exception:
    print("Hubo un error al pasar los argumentos, o estan incompletos")
    sys.exit()

# 2. Obtener los archivos en el directorio indicado
directorios = os.listdir(ruta)

# 3. Vamos a verificar que la extensión, tenga un punto final
if not extension.startswith("."):
    extension = "." + extension

# 4. Recorrer los directorios e identificar los archivos que matcheen
for directorio in directorios:
    if directorio.endswith(extension):
        print(f"Directorio encontrado: {directorio}")
