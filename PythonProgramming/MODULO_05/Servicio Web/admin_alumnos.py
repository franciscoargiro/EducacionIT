# https://alumni.education/course/595/class/2034/297356
# MODULO 5
#    Laboratorio Adicional
#       Ejercicio 1: Administrar alumnos


import  requests

Menu = {
    "1":"Agregar un Alumno.",
    "2":"Modificar alumno existente.",
    "3":"Listar alumnos.",
    "4":"Eliminar alumno."

}


print("Menú")
for key in Menu:
    print(f"{key}. {Menu[key]}")


i=0
while i == 0:
    opcionElegida = input("Seleccione una opcion: ")

    if opcionElegida in Menu:
        # print("ok")
        i=1
    else:
        print("La opción elegida no existe, vuelva a intentar.")


if opcionElegida == "1":
    # AGREGAMOS UN ALUMNO
    nombre = input("Nombre: ")
    cursos = input("Cursos: ")
    r = requests.post("http://localhost:7001/student",
                   json = {
                     "name" : nombre,
                     "courses" : cursos   
                   }     
                )

    print("Código de Estado: ",r.status_code)
    print("Contenido de respuesta: ", r.json())
    print("Alumno ingresado correctamente.")

if opcionElegida == "2":
    # MODIFICAR ALUMNO EXISTENTE
    idAlumnoPorModificar = input("Ingrese el ID del alumno: ")
    nuevoNombre = input("Nuevo nombre (o deje vacío para mantener el anterior: ")
    nuevaCantidadCursos = input("Nueva cantidad de cursos (o deje vacío): ")
    
    if not nuevoNombre and nuevaCantidadCursos:
        datos = {"courses":nuevaCantidadCursos}
    elif nuevoNombre and not nuevaCantidadCursos:
        datos = {"name": nuevoNombre}
    elif nuevoNombre and nuevaCantidadCursos:
        datos = {"courses":nuevaCantidadCursos, "name": nuevoNombre}


    url = "http://localhost:7001/student/"+idAlumnoPorModificar
    
    r = requests.put(url, json=datos)

    codigoEstado = r.status_code
    if codigoEstado == 204:
        print("Se Actualizó correctemante los datos del alumno")
    else:
        print("Algo falló al actualizar los datos del alumno")


if opcionElegida == "3":
    # LISTAR ALUMNOS
    r = requests.get("http://localhost:7001/student")
    if r.status_code == 200:
        respuesta = r.json()
        for alumno in respuesta["students"]:
            print("ID: ",alumno["id"])
            print("Nombre: ",alumno["nombre"])
            print("Cursos: ", alumno["cursos"])
            print("--------------")
    else:
        print("Ocurrio un error al intentar listar los alumnos")  


if opcionElegida == "4":
    # ELIMINAR UN ALUMNO
    idAlumnoPorEliminar =input("Ingrese el ID del alumno: ")

    url = "http://localhost:7001/student/"+idAlumnoPorEliminar

    r = requests.delete(url)

    codigoEstado = r.status_code
    if codigoEstado == 204:
        print("Se Eliminó correctemante al Alumno")
    else:
        print("Algo falló al intentar eliminar al Alumno")