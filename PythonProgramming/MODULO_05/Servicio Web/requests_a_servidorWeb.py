import requests

# r = requests.get("https://customer.cloudmqtt.com/api/instances")

# CONSULTAMOS LOS ALUMNOS CARGADOS EN EL SERVIDOR ------------------------------
"""
r = requests.get("http://localhost:7001/student")
print("Estado: ",r)
respuesta = r.json()
print("Contenido: ",respuesta)
"""



# AGREGAMOS UN ALUMNO AL SERVIDOR ------------------------------------------------
"""
r = requests.post("http://localhost:7001/student",
                   json = {
                     "name" : "Lautaro",
                     "courses" : 3   
                   }     
                )

print("Código de Estado: ",r.status_code)
print("Contenido de respuesta: ", r.json())
"""

# AGREGAMOS VARIOS ALUMNOS AL SERVIDOR ------------------------------------------------

alumnos = {
    ("Ezequiel", 2),
    ("Juan", 4),
    ("Sebastian",5)
}

for nombre, cursos in alumnos:
    r = requests.post("http://localhost:7001/student",
        json= {
            "name": nombre,
            "courses":cursos
        }
   
    )

    print("Código de Estado: ", r.status_code)
    print("Contenido de Rerspuesta: ", r.json())


# UNA VEZ CARGADOS LOS ALUMNOS PODEMOS CONSULTAR CADA UNO DE LOS ALUMNOS CARGADOS CON UN FOR

r = requests.get("http://localhost:7001/student")
if r.status_code == 200:
    respuesta = r.json()
    for alumno in respuesta["students"]:
        print("Alumno: ",alumno["id"])
        print("Nombre: ",alumno["nombre"])
        print("Cursos: ", alumno["cursos"])
        print("--------------")
else:
    print("Ocurrio un error")        


# TAMBIÉN UTILIZANDO LOS RECURSOS DE LA ARQUITECTURA REST PODEMOS ACCEDER A UN ALUMNO EN PARTICULAR ASí http://localhost:7001/student/3
"""    
r = requests.get("http://localhost:7001/student/3")
if r.status_code == 200:
    print(r.json())
    alumno = r.json()
    print("Alumno: ",alumno["id"])
    print("Nombre: ",alumno["nombre"])
    print("Cursos: ",alumno["cursos"])
   
else:
    print("Ocurrio un error")        
"""

# VAMOS A MODIFICAR POR EJEMPLO LA CANTIDAD DE CURSOS DEL ALUMNO 3 CON EL USO DE PUT
"""
datos = {"courses":4} # ---> Acá es importante utilizar la palabra clave del recurso correctemente, es courses en vez de curso, no se de donde la saca
r = requests.put("http://localhost:7001/student/3", json=datos)

codigoEstado = r.status_code
if codigoEstado == 204:
    print("Se Actualizó correctemante los cursos")
else:
    print("Algo falló al actualizar la cantidad de cursos")
"""


# TAMBIÉN PODEMOS MODIFICAR SIMULTANEAMENTE VARIOS DATOS

datos = {"courses":5, "name": "Cesar"} 
r = requests.put("http://localhost:7001/student/3", json=datos)

codigoEstado = r.status_code
if codigoEstado == 204:
    print("Se Actualizó correctemante los cursos")
else:
    print("Algo falló al actualizar la cantidad de cursos")


# POR ULTIMO VAMOS A ELIMINAR UN ALUMNO CON EL MÉTODO DELETE

r = requests.delete("http://localhost:7001/student/0")

codigoEstado = r.status_code
if codigoEstado == 204:
    print("Se Eliminó correctemante al Alumno")
else:
    print("Algo falló al intentar eliminar al Alumno")