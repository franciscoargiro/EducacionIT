import requests


datos = {
    "name":"Francisco",
    "email":"pato@gmail.com",
    "message":"hola Mundo"
}

r = requests.post("http://localhost:8880/form", data=datos)

contenido = r.text

if "Mensaje enviado" in contenido:
    print("Mensaje envíado correctamente")
else:
    print("Error al enviar el mensaje")    
