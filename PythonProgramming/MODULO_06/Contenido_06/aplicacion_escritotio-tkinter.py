import tkinter as tk
from tkinter import PhotoImage, ttk
from PIL import ImageTk, Image
import os


# creamos la ventana principal y le asignamos un título de la siguiente forma
ventana_principal = tk.Tk()
ventana_principal.title("Mi primera aplicación")

# tamaño de la ventana:
ventana_principal.config(width=900, height=450)


# Vamos a crear dos etiquetas, una con texto y otra con imagen
img = ImageTk.PhotoImage(Image.open("MODULO_06\Contenido_06\img.jpg"))
label_img = ttk.Label(image = img)
label_img.place(x=450, y=100)

label = ttk.Label(text="Prueba de Etiqueta")
label.place(x=500, y=50)

# label2 = ttk.Label(Image="mi_icon.ico")


def boton_presionado():
    """funcion de prueba para la presion del boton"""
    print("Haz presionado el Botón")
    boton.config(text="Me presionaste")


# Creamos un boton
boton = tk.Button(text="Hola Mundo", command=boton_presionado)
# boton = ttk.Button(text="Hola Mundo")
boton.place(x=50,y=20)


def imprimir_texto_Entry():
    print(entry1.get())
    entry1.delete(0,tk.END)  # entry1.delete(0,10)  --> o esto borra hasta el caracter número 10 por ejemplo
    entry1.insert(0,"Enviado")

entry1 = ttk.Entry()
entry1.place(x=50, y=100)    

boton_entry1 = tk.Button(text="Imprimir Texto", command=imprimir_texto_Entry)
boton_entry1.place(x=50, y=130)


# ES MUY IMPORTANTE QUE EL mainloop() figure abajo de todo porque justamente es la función que ejecuta el loop
ventana_principal.mainloop()
"""La función mainloop() ejecuta el bucle
principal de la aplicación, aquel encargado de
dibujar todos los controles y manejar los
eventos"""