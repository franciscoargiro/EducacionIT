# importamos el modulo tkinter 
from cgitb import text
import tkinter as tk
from tkinter import ttk

# creamos la estructura basica de la aplicación instanciando la clase y sus atributos
ventana_principal = tk.Tk()
ventana_principal.title("Aplicacion 01")
ventana_principal.config(width=400, height=300)

# Creamos la funcion asociada luego a un botón
def boton01_presionado():
    print("Has presionado el Boton 01")
    boton01.config(text="Hola Mundo")

# Creamos un botón que asociamos a la función anterior    
boton01 = tk.Button(text="Tocar", command=boton01_presionado)
boton01.place(x=50, y=40)


# Construimos una entrada con su botón asociada a una fx para imprimir la entrada

def imprimir_entrada01():
    print(entrada01.get())
    etiqueta01 = tk.Label(text=entrada01.get())
    etiqueta01.place(x=200, y=90)
    entrada01.delete(0, 300)    

entrada01 = tk.Entry()
entrada01.place(x=200, y=40)
boton_entrada01 = tk.Button(text="Imprimir Entrada", command=imprimir_entrada01)
boton_entrada01.place(x=200, y=60)


# >>>> voy por Lista común y desplegable del contenido de Clase 06 <<<<<




# MainLoop mantiene el codigo bloquedo para que se mantenga la ventana abierta 
# hasta cerrarla por tratarse de una aplicacion de escritorio
ventana_principal.mainloop()


