import sys
import tkinter as tk
from tkinter import ttk, messagebox


# FUNCIONES

# Menu
def limpiar():
	entrada_calculo_1.delete(0,tk.END)
	entrada_calculo_2.delete(0,tk.END)
	entrada_resultado.delete(0, tk.END)


def salir():
	resultado = messagebox.askyesno(title="Pregunta",message="¿Desea salir?") 
	if resultado: sys.exit()

def ayuda():
	messagebox.showwarning(title="Advertencia", message="Buena suerte..")

def autor():
	messagebox.showinfo(title="Información", message="Autor: Ezequiel Ustar")


# Operaciones
def convertir(dato):
    try:
        dato = float(dato)
    except:
	    messagebox.showerror( title="¡Error!", message=f"{dato} - No es un valor correcto")
	    dato = "Error"
    return dato

def sumar():
	entrada_resultado.delete(0,tk.END)

	valor_1 = entrada_calculo_1.get()
	valor_1 = convertir(valor_1)
	entrada_calculo_1.delete(0,tk.END)

	valor_2 = entrada_calculo_2.get()
	valor_2 = convertir(valor_2)
	entrada_calculo_2.delete(0,tk.END)

	if valor_1 != "Error" and valor_2 != "Error":
		resultado = valor_1 + valor_2
		lista_de_resultados.insert(tk.END, resultado)
	else:
		resultado = "Error"
	
	entrada_resultado.insert(0, resultado)

def restar():
	entrada_resultado.delete(0,tk.END)

	valor_1 = entrada_calculo_1.get()
	valor_1 = convertir(valor_1)
	entrada_calculo_1.delete(0,tk.END)

	valor_2 = entrada_calculo_2.get()
	valor_2 = convertir(valor_2)
	entrada_calculo_2.delete(0,tk.END)

	if valor_1 != "Error" and valor_2 != "Error":
		resultado = valor_1 - valor_2
		lista_de_resultados.insert(tk.END, resultado)
	else:
		resultado = "Error"
	
	entrada_resultado.insert(0, resultado)

def multiplicar():
	entrada_resultado.delete(0,tk.END)

	valor_1 = entrada_calculo_1.get()
	valor_1 = convertir(valor_1)
	entrada_calculo_1.delete(0,tk.END)

	valor_2 = entrada_calculo_2.get()
	valor_2 = convertir(valor_2)
	entrada_calculo_2.delete(0,tk.END)

	if valor_1 != "Error" and valor_2 != "Error":
		resultado = valor_1 * valor_2
		lista_de_resultados.insert(tk.END, resultado)
	else:
		resultado = "Error"
	
	entrada_resultado.insert(0, resultado)

def dividir():
	entrada_resultado.delete(0,tk.END)

	valor_1 = entrada_calculo_1.get()
	valor_1 = convertir(valor_1)
	entrada_calculo_1.delete(0,tk.END)

	valor_2 = entrada_calculo_2.get()
	valor_2 = convertir(valor_2)
	entrada_calculo_2.delete(0,tk.END)

	if valor_1 != "Error" and valor_2 != "Error" and valor_2 != 0:
		resultado = valor_1 / valor_2
		lista_de_resultados.insert(tk.END, resultado)
	else:
		resultado = "Error"
	
	entrada_resultado.insert(0, resultado)

def resto():
	entrada_resultado.delete(0,tk.END)

	valor_1 = entrada_calculo_1.get()
	valor_1 = convertir(valor_1)
	entrada_calculo_1.delete(0,tk.END)

	valor_2 = entrada_calculo_2.get()
	valor_2 = convertir(valor_2)
	entrada_calculo_2.delete(0,tk.END)

	if valor_1 != "Error" and valor_2 != "Error" and valor_2 != 0:
		resultado = valor_1 % valor_2
		lista_de_resultados.insert(tk.END, resultado)
	else:
		resultado = "Error"
	
	entrada_resultado.insert(0, resultado)

    

# VENTANA
ventana_principal = tk.Tk()
ventana_principal.title("Calculadora Avanzada")


# Menu de navegación
barra_de_menu = tk.Menu()

# SUB MENU
menu_archivo = tk.Menu(barra_de_menu, tearoff=0)
menu_ayuda = tk.Menu(barra_de_menu, tearoff=0)

barra_de_menu.add_cascade(label="Archivo", menu=menu_archivo)
barra_de_menu.add_cascade(label="Referencias", menu=menu_ayuda)

menu_archivo.add_command(label="Limpiar", command=limpiar)
menu_archivo.add_command(label="Salir", command=salir)

menu_ayuda.add_command(label="Ayuda", command=ayuda)
menu_ayuda.add_command(label="Autor", command=autor)



# Botones
boton_suma = ttk.Button(text="+", command=sumar)
boton_suma.place(x=400, y=50)

boton_resta = ttk.Button(text="-", command=restar)
boton_resta .place(x=400, y=100)

boton_multiplicacion = ttk.Button(text="x", command=multiplicar)
boton_multiplicacion.place(x=400, y=150)

boton_division = ttk.Button(text="/", command=dividir)
boton_division.place(x=400, y=200)

boton_resto = ttk.Button(text="%", command=resto)
boton_resto.place(x=400, y=250)


# Entradas
entrada_calculo_1 = ttk.Entry()
entrada_calculo_1.place(x=100, y=50)

entrada_calculo_2 = ttk.Entry()
entrada_calculo_2.place(x=100, y=80)

entrada_resultado = ttk.Entry()
entrada_resultado.place(x=250, y=80)


# Etiquetas
etiqueta_operacion_1 = ttk.Label(text="Valor 1:")
etiqueta_operacion_1.place(x=40, y=50)

etiqueta_operacion_2 = ttk.Label(text="Valor 2:")
etiqueta_operacion_2.place(x=40, y=80)

etiqueta_resultado = ttk.Label(text="Resultado")
etiqueta_resultado.place(x=250, y=50)

etiqueta_acumulados = ttk.Label(text="Resultados acumulados")
etiqueta_acumulados.place(x=100, y=110)


# Listas
lista_de_resultados = tk.Listbox()
lista_de_resultados.place(height=170, width=125 ,x=250, y=110)


# Listas Desplegables
elementos = ["Suma", "Resta", "División", "Multiplicación", "Resto"]
lista_de_operaciones = ttk.Combobox(values=elementos)
lista_de_operaciones.place(width=100, x=100, y=130)


ventana_principal.config(width=500, height=400, menu=barra_de_menu)
ventana_principal.mainloop()