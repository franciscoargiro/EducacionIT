import requests
import pymysql
import sys

criptomoneda = "bitcoin"

# Parte 1 
"""
Creación de la base de datos
- Insertar su valor en una BBDD sqlite3 
    - nombre, divisa, valor

"""


# db_path = r"C:\Users\EducaciónIT\Desktop\EducaciónIT\Cursos\Python Programming\Módulo 6\Ejercicios\Integrador 3\criptos.sqlite"


conn = pymysql.connect(
 host="localhost",
 user="usuario_test",
 passwd="123456",  
 db="test"
)



cursor = conn.cursor()

try:
    cursor.execute("CREATE TABLE `test`.`test_cripto2` ( `cripto_id` INT(7) AUTO_INCREMENT , `nombre` TEXT NOT NULL , `divisa` TEXT NOT NULL , `valor` FLOAT(30) NOT NULL , PRIMARY KEY (`cripto_id`)) ENGINE = InnoDB;")
    # Guardar los cambios.
    conn.commit()

    # conn.close()

except pymysql.err.OperationalError:    #InternalError:
    pass



# Parte 2
"""
Pasaje de argumentos en consola
"""

"""
try:
    argumentos = sys.argv
    criptomoneda = argumentos[1].lower()
except pymysql.err.InternalError:
    print("No se ha ingresado la divisa")
    sys.exit()
"""


# Parte 3
"""
Consultar al servicio web
"""
url = f"https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids={criptomoneda}"
response = requests.get(url)

if response.status_code == 200 and response.json() != []:
    
    diccionario = response.json()[0]
    # id = 1
    nombre = diccionario["name"]
    divisa = "USD"
    valor = float(diccionario["current_price"])



    # Parte 4
    """
    Insertar en Base de Datos
    """

    cursor.execute("INSERT INTO test_cripto2 VALUES (%s, %s, %s)", (nombre, divisa, valor))

    print(f"La cotización de {nombre} es de USD${valor}")

    conn.commit()

    conn.close()


else:
    print("Hubo un error en la consulta del webServise")

