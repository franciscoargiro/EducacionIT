"""
Desarrollar un programa administrar_alumnos.py que permita añadir, modificar, eliminar y listar los alumnos del servicio web.

Estructura: 
{
    'students': []
}

{
    'students': [
        {'courses': 9, 'id': 1, 'name': 'Ezequiel'}, 
        {'courses': 3, 'id': 2, 'name': 'Lautaro'}, 
        {'courses': 1, 'id': 3, 'name': 'Juan'}, 
        {'courses': 5, 'id': 4, 'name': 'Sofía'}, 
        {'courses': 2, 'id': 5, 'name': 'Martín'}
    ]
}
"""
import requests
url = "http://localhost:7001/student"

# 1. Crear un menu
while True:
    print("1. Agregar un alumno.") #post
    print("2. Modificar alumno existente.") #put
    print("3. Listar alumnos.") #get
    print("4. Eliminar un alumno.") #delete
    print("5. Salir")

    opcion = int(input("Opcion: "))

    if opcion == 1:
        nombre = input("Ingrese un nombre: ")
        cursos = int(input("Ingrese la cantidad de cursos que esta haciendo: "))

        response = requests.post(url, json={'name':nombre, 'courses':cursos})
        if response.status_code == 201:
            print("El alumno, se ingreso correctamente")
        else:
            print("El alumno, no se ingreso correctamente")

    elif opcion == 2:
        id = int(input("Ingrese el id del alumno a borrar: "))
        data = {}
    
        nombre = input("Ingrese el nuevo nombre: ")
        
        if nombre != "":
            data['name'] = nombre

        cursos = int(input("Ingrese la nueva cantidad de cursos: "))
        if cursos != "":
            data['courses'] = cursos
        
        response = requests.put(f"http://localhost:7001/student/{id}", json=data)
        if response.status_code == 204:
            print("El alumno se pudo modificar!")
        else:
            print("El alumno NO se pudo modificar!")


    elif opcion == 3:
        response = requests.get("http://localhost:7001/student")

        if response.status_code == 200:
            respuesta = response.json()
            for alumno in respuesta["students"]:
                print("Alumno", alumno["id"])
                print("Nombre:", alumno["name"])
                print("Cursos:", alumno["courses"])

    elif opcion == 4:
        id = int(input("Ingrese el id del alumno a borrar: "))
        response = requests.delete("http://localhost:7001/student" + f"/{id}")

        if response.status_code == 204:
            print("Alumno eliminado correctamente!")
        else:
            print("Alumno no encontrado!")


    elif opcion == 5:
        break
    else:
        break
    

