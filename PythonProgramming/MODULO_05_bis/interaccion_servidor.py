import requests

# GET
"""
El método GET, en la arquitectura REST, quiere decir “obtener una lista de los registros de un recurso”.

. Por lo general los recursos de los servicios web estarán escritos en inglés.
. Este recurso permite obtener, agregar, modificar y eliminar alumnos (students).
. Cada alumno del recurso tiene un nombre (name) y una cantidad de cursos realizados (courses). 
"""

response = requests.get("http://localhost:7001/student")

print("Código de estado:", response.status_code)
print("Contenido de la respuesta:", response.json())

"""
response.json()
Lee el contenido de la respuesta del servidor en formato JSON y lo convierte a las estructuras de Python correspondientes.

Por lo pronto la lista de alumnos se encuentra vacia.
"""



# POST
"""
Ya vimos que el método HTTP para agregar información a un recurso es POST. 

. En el caso de nuestro servicio de prueba, lo estaremos usando para insertar nuevos alumnos.
. Dijimos que cada alumno tiene:
    - Un nombre
    - Una cantidad de cursos.
"""
response = requests.post("http://localhost:7001/student", json={"name": "Lautaro", "courses": 3})

print("Código de estado:", response.status_code)
print("Contenido de la respuesta:", response.json())

"""
El código 201 indica que se insertó satisfactoriamente un nuevo registro a un recurso. 

. El diccionario {"id": 1} indica que el servicio le asignó el número 1 al alumno que acabamos de crear. 
. Este identificador nos será útil luego para hacer modificaciones sobre ese alumno e incluso eliminarlo.

Agreguemos algunos alumnos más para tener más de un registro.
"""

alumnos = (
    ("Juan", 1),
    ("Sofía", 5),
    ("Martín", 2)
)
for nombre, cursos in alumnos:
    response = requests.post("http://localhost:7001/student", json={"name": nombre, "courses": cursos})

    print("Código de estado:", response.status_code)
    print("Contenido de la respuesta:", response.json())



# GET
"""
Ahora que tenemos algunos alumnos, si obtenemos la lista de alumnos:
"""

response = requests.get("http://localhost:7001/student")
print("Código de estado:", response.status_code)

respuesta = response.json()
print("Contenido de la respuesta:", respuesta)

"""
Ahora el contenido de respuesta["students"] ya no es una lista vacía. 
Cada uno de sus elementos representa a un alumno como un diccionario. 

Cada alumno tiene tres claves: 
    - Un identificador numérico ("id")
    - Un nombre ("name")
    - Una cantidad de cursos ("courses").

Podemos recorrer cada uno de los alumnos usando un bucle “for”
** Ver en navegador
"""

response = requests.get("http://localhost:7001/student")

if response.status_code == 200:
    
    respuesta = response.json()
    for alumno in respuesta["students"]:
        print("Alumno", alumno["id"])
        print("Nombre:", alumno["name"])
        print("Cursos:", alumno["courses"])

else:
    print("Ocurrió un error.")

"""
Podemos obtener la información de algún alumno en particular indicando su identificador numérico. 

GET /student/3 -> debe retornar únicamente la información de Sofía.
"""

response = requests.get("http://localhost:7001/student/3")
print("Código de estado:", response.status_code)
print("Contenido de la respuesta:", response.json())


"""
La información del alumno está dentro de la clave "student".
"""

response = requests.get("http://localhost:7001/student/3")

if response.status_code == 200:

    alumno = response.json()["student"]
    print("Nombre:", alumno["name"])
    print("Cursos:", alumno["courses"])

else:
    print("Ocurrió un error.")



# PUT
"""
¿Cómo proceder si queremos modificar algún dato (nombre o cantidad de cursos) del alumno 3 (Sofía)? 

Según lo que vimos más arriba, debemos usar el método PUT. 
. requests.put()
    - Vamos a pasar como argumento un diccionario con las claves que queremos modificar y sus respectivos valores,
      que luego serán convertidas a formato JSON automáticamente por la librería.
"""

datos = {"courses": 6}
response = requests.put("http://localhost:7001/student/3", json=datos)

print("Código de estado:", response.status_code)


"""
El código de estado 204 indica que la petición se ejecutó correctamente, no retorna ningún contenido. 
Por eso no llamamos a r.json().

Si la petición fallara por alguna razón, su código de estado sería otro que 204. 
"""

datos = {"courses": 6}
response = requests.put("http://localhost:7001/student/10", json=datos)

print("Código de estado:", response.status_code)


"""
El código de estado 404 indica que no se encontró el recurso sobre el cual se quiere ejecutar la operación. 

En este caso, efectivamente, no hay ningún alumno cuyo identificador sea el número 10.

Al invocar la función put(), es posible modificar varias claves simultáneamente:
"""
# Modifica el nombre y la cantidad de cursos del alumno 3.
datos = {"name": "Josefina", "courses": 6}
r = requests.put("http://localhost:7001/student/3", json=datos)
print("Código de estado:", r.status_code)



# DELETE
"""
Para eliminar un registro en un recurso usamos el método DELETE del protocolo HTTP: 
"""

# Elimina el alumno 3.
r = requests.delete("http://localhost:7001/student/3")
print("Código de estado:", r.status_code)