# Vamos a probar como funciona la funcion filter() que nos permite filtar una lista de iterables según cumplan una condición True

# Definimos la funcion
def compruebo(i):
    if i%2 == 0:
        return True

lista01 = [2,5,10,65,81,99,105,202]


lista02 = filter(compruebo,lista01)

print(lista02) # aca veremos que filter al igual que map lo que genera es un objeto que para poder imprimirlo debemos convertirlo antes a lista

print(list(lista02))