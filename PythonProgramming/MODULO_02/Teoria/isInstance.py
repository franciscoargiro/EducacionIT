"""
Utilizando la función incorporada isinstance() podemos
chequear si un objeto es de un tipo de dato determinado.
"""

def dividir(a,b):
    if not isinstance(a,(int,float)) or not isinstance(b,(int,float)) or b < 0:
        raise TypeError("No es un úmero o es menor a cero")
    return a/b


print ("Prueba 1: ")
print(dividir(10,5))  

print ("Prueba 2: ")
print(dividir(10,-1)) 

print ("Prueba 3: ")
print(dividir(10,"a"))            