# Vamos a probar una funcion generica de captura de errores


try:
    int("Hola Mundo")
except Exception:
    print("Algo Salió mal")
else:
    print("El entero fue convertido correctamente")


try:
    int("4")
except Exception:
    print("Algo Salió mal")
else:
    print("El entero fue convertido correctamente")