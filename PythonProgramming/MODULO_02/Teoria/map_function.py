"""
Vamos a convertir la siguiente lista de string en una lista de enteros con map()
"""

str_nums = ["4", "8", "6", "5", "3", "2", "8", "9", "2", "5"]

int_nums = map(int,str_nums)

lista_int_nums = list(int_nums)   # al utilizar map() nos devuelve un objeto, por lo que si queremos trabajar como lista tenemos que transformarlo en con list()


print(lista_int_nums)


# Ahora vamos a tomar la lista de enteros y elevar al cuadrado cada uno de sus items, 
# pero vamos hacerlo de dos formas distintas: una con un for y otra con map()

al_cuadrado_con_For = []

for nums in lista_int_nums:
    al_cuadrado_con_For.append(nums ** 2)

print(list(al_cuadrado_con_For)) 


## Ahora vamos a probar de hacer lo mismo pero con map() nuevamente

def alCuadrado(i):
    return i ** 2

al_cuadrado_con_map = map(alCuadrado,lista_int_nums)

print(list(al_cuadrado_con_map)) 

