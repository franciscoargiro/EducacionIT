"""
Otra operación bastante común es la de querer
eliminar los espacios iniciales y finales de una
cadena. Por ejemplo, si le solicitamos el nombre a
un usuario a través de la consola (vía la función
input()) y accidentalmente ingresa un espacio
al final, antes de presionar la tecla Enter. En ese
caso tendríamos una cadena como la siguiente:
>>> nombre = "Juan "
>>> nombre.strip()
'Juan'
Pero también .strip() nos puede ser útil para
quitar caracteres especiales del principio o del
final de un str.
>>> dato = "\t\nHola\nChau"
>>> print(dato.strip())
Hola
Chau
Borro el \t\n del principio, pero no manipulo el
\n del medio. Si hubiera estado al final, lo
hubiera borrado también. Esto es especialmente
útil cuando trabajemos con los archivos texto
(txt), podremos eliminar el salto de línea al leer
renglones.

"""

dato= "\t\nHola\nChau"
print(dato.strip())

print(dato.swapcase())

dato = dato.lower()

print(dato)

print(dato.swapcase())

print(dato.title())

print(dato.capitalize())
