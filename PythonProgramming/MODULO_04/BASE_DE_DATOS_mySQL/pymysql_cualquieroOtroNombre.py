
import pymysql
# import pymysql.cursors


conn = pymysql.connect(
 host="localhost",
 user="usuario_test",
 passwd="123456",  
 db="test"
)



cursor = conn.cursor()


# cursor.execute("CREATE TABLE `test`.`personas` ( `personas_id` INT(7) NULL AUTO_INCREMENT , `nombre` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL , `edad` INT(3) NOT NULL , PRIMARY KEY (`personas_id`)) ENGINE = InnoDB;")
"""
Comentamos el execute() anterior porque la tabla solo se crea una vez
para lo cual utilizamos la posibilidad de capturar el error:
"""
try:
 cursor.execute("CREATE TABLE `test`.`personas` ( `personas_id` INT(7) NULL AUTO_INCREMENT , `nombre` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL , `edad` INT(3) NOT NULL , PRIMARY KEY (`personas_id`)) ENGINE = InnoDB;")
except pymysql.err.InternalError:
 pass
 # print("La consulta no se ejecutó correctamente, es probable que ya exista la Tabla.")



personas = (
 ("Pablo", 30),
 ("Jorge", 41),
 ("Pedro", 27)
)

for nombre, edad in personas:
    cursor.execute("INSERT INTO personas VALUES (%s, %s)", (nombre, edad))


try:
    cursor.execute("SELECT apellido FROM personas")
except pymysql.err.InternalError:
    print("La consulta no se ejecutó correctamente.")


cursor.execute("SELECT * FROM personas")
personas = cursor.fetchall()
print(personas)


"""
try:
   cursor.execute("SELECT * FROM personas")
    personas = cursor.fetchall()
    print(personas)
except pymysql.err.InternalError:
    print("La consulta no se ejecutó correctamente.")
"""


# Guardar los cambios.
conn.commit()

conn.close()
