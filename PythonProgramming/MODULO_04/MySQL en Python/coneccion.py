import pymysql

# conexión a BD
conn = pymysql.connect(
 host="localhost",
 user="root",
 password="",  
 db="test_bitcoin"
)

# creamos el cursor
cursor = conn.cursor()

personas = (
 ("Pablo", 30),
 ("Jorge", 41),
 ("Pedro", 27)
)


# CREATE TABLE

"""
try:
    cursor.execute("CREATE TABLE `test_bitcoin`.`personas` ( `personas_id` INT(7) NULL AUTO_INCREMENT , `nombre` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL , `edad` INT(3) NOT NULL , PRIMARY KEY (`personas_id`)) ENGINE = InnoDB;")
except pymysql.err.OperationalError:
    print("La Tabla con ese nombre ya existe.")

"""


# INSERT

for nombre, edad in personas:
    cursor.execute("INSERT INTO `test_bitcoin`.`personas` (`nombre`, `edad`) VALUES (%s, %s)", (nombre, edad))  # este código de esta forma evita las inyecciones, por unos filtros internos que por defecto trae pymysql
   
    
# importante comitear la consulta y cerrar la conexión


conn.commit()

conn.close() 





