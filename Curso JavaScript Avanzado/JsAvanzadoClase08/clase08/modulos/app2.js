//OPCION 1 - Exponer en window
/*(function(){
	function Zoo(zona){
		this.zona = zona;
		this.animales = [];
		this.agregarAnimales = function(animal){
			if(animal instanceof crearAnimal){
				this.animales.push(animal)
			}
			else{
				console.error(animal, 'No es una instancia de Animal');
			}
		}
	}

	//Expongo funcionalidad
	window.crearZoo = Zoo;

})();
*/
//OPCION 2 - Exponer "namespace"
var ZOO = (function(zona){
	return {
		crearZoo : function(zona){
			return {
				zona : zona,
				animales : [],
				agregarAnimales : function(animal){
					this.animales.push(animal);
				}
			}
		}
	}
})();