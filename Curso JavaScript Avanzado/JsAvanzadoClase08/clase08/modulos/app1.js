//OPCION 1 - Exponer en window
/*(function(){
	//Creo un objeto tipo Animal
	function Animal(nombre,saludo,cantPatas){
		this.vive = true;
		this.nombre = nombre;
		this.saludar = function(){console.info(saludo);}
		if (cantPatas != undefined){
			this.cantPatas = cantPatas;
		}
		else{
			this.cantPatas = 4;	
		}
	}

	//Expongo la funcion en window
	window.crearAnimal = Animal;
})();*/

//OPCION 2 - Exponer "namespace"
var ANIMAL = (function(nombre,saludo,cantPatas){
	return {
		crearAnimal : function(nombre,saludo,cantPatas){
			return {
				vive : true,
				nombre : nombre,
				cantPatas : cantPatas,
				saludo : saludo,
				saludar : function(){console.info(saludo);}
			}
		}
	}
})();