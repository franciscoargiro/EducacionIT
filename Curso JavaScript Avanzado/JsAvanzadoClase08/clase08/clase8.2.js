//Extender Prototipos

String.prototype.reverse = function(){
	return this.split('').reverse().join('');
}

String.prototype.esPalindromo = function(){
	var lower = this.toLowerCase();
	return lower.toString() === lower.reverse();
}