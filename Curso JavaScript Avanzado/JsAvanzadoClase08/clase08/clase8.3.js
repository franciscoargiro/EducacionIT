//Mismo ejemplo, aplico composicion

//Constructor
function Animal(){
	this.vive = true;
	this.saludar = function(){console.log('saludo')};
}

//Constructor
function Mascota(nombre){
	Animal.call(this);
	this.nombre = nombre;
	this.irVet = function(){return true};
}

//Constructor
function Gato(nombre,raza){
	Mascota.call(this,nombre);
	this.raza = raza;
	this.saludar = function(){console.log('miaauuuuu')};
}


///////////////////////////////////////////////////////

var gato = new Gato('Garfield','De la calle');