//Constructor
function Animal(){
	this.vive = true;
	this.saludar = function(){console.log('saludo')};
}

//Constructor
function Mascota(nombre){
	this.nombre = nombre;
	this.irVet = function(){return true};
}

Mascota.prototype = new Animal();

//Constructor
function Gato(nombre,raza){
	//this.prototype = Object.create(Mascota.prototype);
	Mascota.call(this,nombre);
	this.raza = raza;
	this.saludar = function(){console.log('miaauuuuu')};
}

Gato.prototype = new Mascota();

///////////////////////////////////////////////////////

var gato = new Gato('Garfield','De la calle');