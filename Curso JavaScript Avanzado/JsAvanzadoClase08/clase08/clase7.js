// //Clase.7.JS.Avanzado

// //1 - Entorno o Contexto Global en el navegador
// //console.log('Hola',window);

// var nombre = "Mario";
// saludar();

// //Controlo si una propiedad o funcion ya existe en el contexto global
// /*if(window.saludar){
// 	console.log('No la sobreescribo');
// }*/

// function saludar(){
// 	var saludo = "Hola!";
// 	var nombre = "Pepe";
// 	apellido = "Romero";
// 	console.info(saludo,nombre);
// }
// // FIN - 1

//2 - Funciones dentro de funciones y objeto this
var nombre = "";
saludar();

function saludar(){

	var saludo = "Hola!";
	console.log(nombreGenerico(nombre));

	function nombreGenerico(nombre){
		var generico = "Amigo";
		//var saludo = "Hey!";

		if(nombre===""){
			nombre = generico;
		}
		console.info('This',this);
		return saludo+' '+nombre;
	}
}

var persona = {
	id: 10,
	nombre:'Mario',
	apellido:'Romero',
	saludar: function(){
		console.info('Persona',this);
	}
}

persona.saludar();
