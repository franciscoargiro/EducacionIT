

// Declaro un objeto Json

var helado = {
	sabor: 'vainilla',
	kg: '2',
	local: 'Mabecar'

}


// Serializamos el objeto, que es como tomar una foto al momento del objeto

var heladoSerializado = JSON.stringify(helado);


// modifificamos el objeto 
helado.sabor= 'chocolate';
console.log(helado.sabor);



// al deserializar lo que vemos es que el objeto es una imagen del momento anterior a modificarlo

helado = JSON.parse(heladoSerializado);
console.log(helado.sabor);