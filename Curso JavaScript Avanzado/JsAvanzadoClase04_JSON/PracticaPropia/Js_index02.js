
// hay que tener cuidado con eval porque interpereta operaciones y funciones con String
var operacion = '4 + 5+ 1';
var resultado = eval(operacion);
console.log(resultado);



// JSON esctricto es con comillas dobles para las claves de un objeto salvo los int

//var pepe = JSON.parse('{nombre: "Pepe", edad: 22}');  // Esto nos va a arrojar un error
//console.log(pepe.edad);

var rene = JSON.parse('{"nombre": "Rene", "edad":22}'); // Esto da OK
console.log(rene.edad);