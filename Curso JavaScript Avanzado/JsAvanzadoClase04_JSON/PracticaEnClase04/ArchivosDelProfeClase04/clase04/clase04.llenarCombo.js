//Llenar combo

var data = [
		{id: 1 , nombre:"Breaking Bad"},
		{id: 2 , nombre:"Lost"},
		{id: 3 , nombre:"GOT"},
		{id: 4 , nombre:"Twin Peaks"},
		{id: 5 , nombre:"TBBT"}
	];

//1-DOM
var btn = document.querySelector('#btn');
var sel = document.querySelector('#series');

//2-Vincularnos a Eventos
btn.addEventListener('click',llenarCombo);

dale(null,null); //Error: undefined

//3-Callbacks
function llenarCombo(){

	btn.removeEventListener('click',llenarCombo);
	//dale(null,null); //OK!

	//Version 1
	/*
	for(var i=0;i<data.length;i++){
		var elemento = document.createElement('option');
		elemento.value = data[i].id;
		elemento.innerText = data[i].nombre;
		sel.appendChild(elemento);
	}*/

	//Version 2
	/*data.forEach(function(e,index){
		var elemento = document.createElement('option');
		elemento.value = e.id;
		elemento.innerText = e.nombre;
		sel.appendChild(elemento);
	});*/

	//Version 3. Defino una funcion (callback) dentro de otra
	data.forEach(dale);
	function dale(e,index){
		console.log('index',index);
		var elemento = document.createElement('option');
		elemento.value = e.id;
		elemento.innerText = e.nombre;
		sel.appendChild(elemento);
	}

	

}

