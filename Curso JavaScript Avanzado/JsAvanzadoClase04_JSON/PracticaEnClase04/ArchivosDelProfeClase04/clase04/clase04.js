//Ejemplos JSON
var numeros = ['uno', 'dos', 'tres'];
var clima = { dia: 'lunes', temp: 24, desc: 'despejado'};

var pronostico = [
	{ dia: 'martes', min: 5, max: 24, desc: 'despejado'},
	{ dia: 'miercoles', min: -3, max: 12, desc: 'nublado'},
	{ dia: 'jueves', min: 4, max: 18, desc: 'nublado'}
];

var todo = {
	hoy: clima,
	pronostico: pronostico,
	cant: 1	
};


//Serializar
/*var jsonTxt = JSON.stringify(todo);
console.log(jsonTxt);
console.log(JSON.parse(jsonTxt));
*/

//+Ejemplo Serializar

var txt = JSON.stringify(clima);
clima.dia = "jueves";
clima.temp = 33;
var nuevoClima = JSON.parse(txt);

console.log('clima:',clima,'nuevoClima:',nuevoClima);

