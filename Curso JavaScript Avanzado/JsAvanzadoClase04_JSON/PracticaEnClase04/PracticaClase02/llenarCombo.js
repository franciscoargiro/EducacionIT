//llenar combo

// creamos la variable data para utilizar JSON que va a ser un array de objetos
var data = [
	{id: 1, nombre: "Bracking Bad", },
	{id: 2, nombre: "Orange", },
	{id: 3, nombre: "Pirates", },
	{id: 4, nombre: "Narcos", },
	
]


// Seleccionamos los elementos del DOM
var btn = document.querySelector("#btn");
var sel = document.querySelector("#series");

btn.addEventListener("click", llenarCombo);

function llenarCombo() {

	
	btn.removeEventListener("click", llenarCombo); // Esto es para que no se repita al hacer doble click
	
	/*
	// PRIMERA FORMA DE RECORRER EL ARREGLO
	for(var i=0; i<data.length;i++) {
		var elemento = document.createElement("option");
		elemento.value = data[i].id;
		elemento.innerText = data[i].nombre;
		sel.appendChild(elemento);
	} 
	*/


	// SEGUNDA FORMA DE RECORRER EL ARREGLO 
	// e -> por element
	data.forEach( function(e, index) {
		var elemento = document.createElement('option');
		elemento.value =e.id;
		elemento.innerText = e.nombre;
		sel.appendChild(elemento);
	});

}