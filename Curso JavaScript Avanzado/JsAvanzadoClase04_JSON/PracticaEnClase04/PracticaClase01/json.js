
// con [] definimos arrays y con {} definimos objetos


// un array con variables
var numeros = ['uno', 'dos', 'tres'];  

// un objeto con sus variables y valores
var clima = { dia: 'lunes', temp: 24, desc: 'despejado'};

var pronostico = [  // un array de objetos
	{ dia: 'martes', min: 5, max: 24, desc: 'despejado'},
	{ dia: 'miercoles', min: -3, max: 12, desc: 'nublado'},
	{ dia: 'jueves', min: 4, max: 18, desc: 'nublado'}
];

var todo = {  // un objeto con objetos dentros
	hoy: clima,
	pronostico: pronostico
};


// Serializamos
/*
var jsonTxt = JSON.stringify(todo);
console.log(jsonTxt);
console.log(JSON.parse(jsonTxt));
*/

var txt = JSON.stringify(clima);
clima.dia= "jueves";
clima.temp= 33;
var nuevoClima = JSON.parse(txt);

console.log(clima);
console.log(nuevoClima);


