console.log('Arrancamos!');

//1- Elementos del DOM
var btn = document.querySelector('#grabar');
var txt = document.querySelector('#nombre');
var sel = document.querySelector('#pais');
var p = document.querySelector('.c1');
var frm = document.querySelector('#myform');

//En JS podemos asignar una funcion a una variable
var myfunc = demo2;

var myanon = function (){
	console.log('Soy una func anonima!');
}

//2 - Vincularnos con sus eventos

/* Vincularnos v1
btn.onclick = demo;
txt.onfocus = demo;
sel.onchange = demoSelect;
*/

//Vincularnos v2 - addEventListener
btn.addEventListener('click',demo);
btn.addEventListener('click',demo2);
btn.addEventListener('click',cambiarColor);
txt.addEventListener('focus',demo);
sel.addEventListener('change',demoSelect);
frm.addEventListener('submit',validar);
txt.addEventListener('blur',function(){
	console.log('Soy un codigo anonimo!');
});

//Callbacks
function demo(){
	var hola = "Hola JS! version 2";
	console.log(hola);
	//Remuevo callback demo
	//btn.removeEventListener('click',demo);
}

function demoSelect(){
	//Puedo utilizar la var sel creada fuera de la funcion, ya que por default es una variable "global"
	//var valor = document.querySelector('#pais');
	console.log('Valor v2:',sel.value);
}

function demo2(){
	console.log('Soy Demo2');
}

function cambiarColor(){
	p.classList.remove('c1');
	p.classList.add('c2');
	p.style="color:red";
}

function validar(e){

	myfunc(); //llamada a una funcion en una varible
	myanon(); //llamada a una funcion anonima (que guarde en una variable)

	if(txt.value !== ""){//Validar que exista un nombre ingresado antes del submit
		console.log('Todo OK!');
	}
	else{
		p.innerHTML = "Por favor, ingrese un nombre";
		e.preventDefault();
	}
}