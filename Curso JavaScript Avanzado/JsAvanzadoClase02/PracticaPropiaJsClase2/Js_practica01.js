
// seleccionamos los elementos del DOM y los colocamos en variables

var btnProductos = document.querySelector("#js_productos");
var btnHistoria = document.querySelector("#js_historia");
var btnContacto = document.querySelector("#js_contacto");


// relacionamos los elemntos a un evento y una funcion

btnProductos.addEventListener("click",productosFx);
	btnProductos.addEventListener("click",mostrarEvento);

btnHistoria.addEventListener("click",historiaFx);
	var n = 0;

btnContacto.addEventListener("click",contactoFx);




// ahora creo las funciones correspondientes


function productosFx() {
	console.log('Tocaste el boton de Productos');
}

	function mostrarEvento(e){
		console.log(e);
	}


function historiaFx() {
	console.log("Tocaste el boton de la Historia", n++);

	if (n == 3) {
		//padre = btnHistoria.parentNode;
		//padre.removeChild(btnHistoria);
		btnHistoria.parentNode.removeChild(btnHistoria);  // Al tocar 3 veces el boton Historia lo remuevo
		console.log('Estas viendo mucha historia!! ;p');
	}

}


function contactoFx() {
	console.log("Tocaste el boton para contacto");

}


// -------- Laboratorio 1 - Clase 02 - Validar campo repetir email -----------

var email1 = document.querySelector("#Js_mail1");
var email2 = document.querySelector("#Js_mail2");

var formulario = document.querySelector("#Js_formulario");

email2.addEventListener("blur",validarFx);

	function validarFx(){

		if(email1.value == email2.value){
			console.log("Los mails coinciden");
			formulario.classList.remove('bordeRojo');
			formulario.classList.add('bordeVerde');


		} else {
			console.log("NO coinciden los correos");
			formulario.classList.add('bordeRojo');
			formulario.classList.remove('bordeVerde');
			email1.addEventListener("blur",validarFx);

		}

	}


// -------- Laboratorio 2 - Clase 02 - Selector de Opciones -----------
