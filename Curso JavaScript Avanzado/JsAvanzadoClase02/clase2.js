
// nos posicionamos en el elemento
var email = document.querySelector("#email");
var email2 = document.querySelector("#email2");


// tomamos el evento y le asociamos una funcion
email2.addEventListener('blur',validar); // blur es como el evento onBlur 


// generamos la funcion que se va a ejecutar con el evento
function validar(){
	if (email.value === email2.value) {
		email2.classList.remove('error');   // esta linea es para resetear elemento a como estaba al remover la clase
		email2.classList.add('ok');
	} else {
		email2.classList.remove('ok');   // esta linea es para resetear elemento a como estaba al remover la clase
		email2.classList.add('error');
	}
}


