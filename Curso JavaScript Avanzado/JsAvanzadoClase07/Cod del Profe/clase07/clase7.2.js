// //This, that, me, self
// function Persona(nombre, edad){
// 	var clave = '1234';
// 	var self = this;
// 	this.nombre = nombre;
// 	this.edad = edad;
// 	this.saludar = saludar;
// 	init();

// 	function saludar(){
// 		console.log('Hola, soy %s', this.nombre);
// 		//console.log('contexto saludar:',this);
// 	}

// 	function init(){
// 		//console.log('contexto:',this);
// 		if(!self.edad) self.edad = 18;
// 	}
// }

// var a = new Persona('Sin edad');
// console.log(a.edad);
// var b = new Persona('Pepe',20);
// console.log(b.edad);

//Redefinir el contexto
function Persona(nombre, edad){
	var clave = '1234';
	this.nombre = nombre;
	this.edad = edad;
	this.saludar = saludar;
	this.saludarGenerico = saludar.bind(this,'amigo');
	init.call(this);

	function saludar(val){
		console.log('Hola %s, soy %s',val, this.nombre);
		//console.log('contexto saludar:',this);
	}

	function init(){
		//console.log('contexto:',this);
		if(!this.edad) this.edad = 18;
	}
}

var a = new Persona('Sin edad');
console.log(a.edad);
var b = new Persona('Pepe',20);
console.log(b.edad);