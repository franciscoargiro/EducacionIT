//Funcion que devuelve un objeto
function crearPersona(nombre, edad){
	var obj = {};
	var inaccesible = 'clave secreta';
	obj.nombre = nombre;
	obj.edad = edad;
	obj.saludar = saludar;
	
	return obj;

	function saludar(){
		console.log('Hola, soy %s', this.nombre);
		_funcionPrivada();
	}

	function _funcionPrivada(){
		console.log('Nadie me ve');
	}
}

var a = crearPersona('Sr Uno', 23);
a.saludar();
var b = crearPersona('Sr Dos', 32);
b.saludar()