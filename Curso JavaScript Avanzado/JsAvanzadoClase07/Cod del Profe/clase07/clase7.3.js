//Funciones anonimas autoejecutadas

(function(a){
	var nombre = "Mario";
	var mensaje = "Hola!";

	console.log(mensaje+' '+nombre+' ');

})('Curso');

//Al no estar protegido dentro de la funcion anonima, cuelga de window (objeto global)
var mensaje2="cosas";

//Me aseguro de utilizar la funcion $ como jQuery
(function($){
	$('');
})(jQuery);