//Clase3

//pedido es el objeto que me permite trabajar con comunicaciones asincronicas (Ajax)
var pedido = new XMLHttpRequest();
//var url = 'demo.txt';
var url = 'https://jsonplaceholder.typicode.com/posts/1';
//var url = 'http://www.mercadolibre.com.ar';

//1 - Pos en el DOM
var btn = document.querySelector('#btn');

//2 - Vincularme al Evento
btn.addEventListener('click',enviarPedidoAjax);

//3 - Callbacks
function enviarPedidoAjax(){
	//Abro la conexion
	pedido.open('GET',url);
	//Me vinculo al evento load del objecto XHR
	pedido.addEventListener('load',recibirPedido);
	//Enviamos el pedido
	pedido.send();
	//console.log('XHR test',pedido.responseText);
}

function enviarPedidoXHR(){
	pedido.open('GET',url,false);
	pedido.send();
	console.log(pedido.responseText);
}

//Procesamos respuesta del pedido
function recibirPedido(){
	console.log(pedido.responseText);
}