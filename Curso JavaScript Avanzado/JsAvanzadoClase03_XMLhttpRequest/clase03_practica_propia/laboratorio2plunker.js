console.clear();

//selecciono elementos
var provincia = document.querySelector('---');  // No se como elegir provincia de varias opciones
var localidad = document.querySelector('---');  // idem anterior 

//variables globales
var pedido; //va a guardar el XHR

//vinculo a eventos
provincia.addEventListener('change', cargarLocalidad);

//funciones
function cargarLocalidades(){
  
  var idProvincia = provincia.value; //obtengo el valor del select provincia
  console.log(idProvincia);
  
  var url = 'ajax/localidad/'+idProvincia;
  
  pedido = new XMLHttpRequest(); //creo el XHR
  pedido.addEventListener('load', ajaxResponse); //me suscribo al evento load con la funcion ajaxResponse
  pedido.addEventListener('error', ajaxError); //me suscribo al evento error con la funcion ajaxError
  pedido.open('GET', url); //abro una conexión GET utilizando la url 
  pedido.send();
  
  localidad.innerHTML = '---'; // "limpio" el select de localidades
}

function dibujarLocalidades(optionsHTML){
  localidad.innerHTML = optionsHTML;
}

function ajaxResponse(){
  var serverOk = pedido.status == 200; //valido que no haya errores de servidor
  if(!serverOk){
    ajaxError();
  } else {
    var respuesta = pedido.responseText; //obtengo la respuesta de ajax
    dibujarLocalidades(respuesta); //llamar a la funcion dibujarLocalidades con el HTML obtenido
  }
}

function ajaxError(){
  console.warn('ajax error');
}