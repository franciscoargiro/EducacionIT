//selecciono elementos
var campo = document.querySelector('input[name=user]');



//variables globales
var pedido; //va a guardar el XHR

//vinculo a eventos
campo.addEventListener('blur', enviarUsuario);
campo.addEventListener('click', limpiarClases);

//funciones
function limpiarClases(){
  campo.classList.remove('error');
  campo.classList.remove('ok');
}
function enviarUsuario(){
  
  var usuario = campo.value; //obtengo el valor del campo
  console.log(campo.value); // Chequeo en consola que este bien el vlaue
  
  var url = 'validar.php?user='+usuario; // 'validar.php?user=pepe'
  
  pedido = new XMLHttpRequest(); //creo el XHR
  pedido.addEventListener('load', ajaxResponde); //me suscribo al evento load con la funcion ajaxResponse
  pedido.addEventListener('error', ajaxError); //me suscribo al evento error con la funcion ajaxError
  pedido.open('GET', url); //abro una conexión GET utilizando la url 
  pedido.send();
}

function ajaxResponse(){
  console.log(pedido.status); // Muestro en consola el status para chequearlo
  var serverOk = pedido.status == 200; //valido que no haya errores de servidor, por el momento pongo true porque estoy simulando el servidor
  if(!serverOk){
    ajaxError();
  } else {
    var respuesta = pedido.responseText; //obtengo la respuesta de ajax
    console.log('el server respondio ', respuesta, 'tipo ', typeof respuesta);
    //si la respuesta es true agregarle la clase "ok"
    if(respuesta === 'true'){
      campo.classList.add('ok');
    } else {
      campo.classList.add('error');
    }
    //si la respuesta es false agregarle la clase "error"
  }
}

function ajaxError(){
  console.warn('ajax error');
}