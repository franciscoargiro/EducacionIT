(function($,app,_productos,window){

app.Listado = {

	init : function(){
		//$.getJSON('productos').done(listadorecibirProductos); //Endpoint json "productos"
		_productos.data.forEach(this.listadoDibujarListado.bind(this));
	},

	/*listadoInit = function(){
		//$.getJSON('productos').done(listadorecibirProductos); //Endpoint json "productos"
		productos.data.forEach(listadoDibujarListado);
	};*/

	listadorecibirProductos : function(data){
		data.data.forEach(this.listadoDibujarListado.bind(this));
	},

	/*listadorecibirProductos = function(data){
		data.data.forEach(listadoDibujarListado); //Inspeccionar el json que devuelve el servidor para saber como utilizarlo
	};*/

	listadoDibujarListado : function(unProducto,index){
		var $item = $($('#catalog__item__template').html()); //Tomo el html del template "catalog__item__template" como texto para crear un objeto jQuery
		var $catalogo = $('.catalog');
	    $('.title', $item).html(unProducto.titulo);
	    $('.description', $item).html(unProducto.descripcion);
	    $('.price span', $item).html(unProducto.precio);
	    $('img', $item).attr('src', unProducto.imagen);
	    $('img', $item).load(showItem);
	    $catalogo.append($item);
	    
	    $item.hide();
	    $('button', $item).click(agregar);

	    function agregar(){
	    	app.Pedido.pedidoAgregar(unProducto);
	    }

	    function showItem(){
	    	$item.delay(index*200).fadeIn(500);
	    }
	}

	/*listadoDibujarListado = function(unProducto,index){
		var $item = $($('#catalog__item__template').html()); //Tomo el html del template "catalog__item__template" como texto para crear un objeto jQuery
		var $catalogo = $('.catalog');
	    $('.title', $item).html(unProducto.titulo);
	    $('.description', $item).html(unProducto.descripcion);
	    $('.price span', $item).html(unProducto.precio);
	    $('img', $item).attr('src', unProducto.imagen);
	    $('img', $item).load(showItem);
	    $catalogo.append($item);
	    
	    $item.hide();
	    $('button', $item).click(agregar);

	    function agregar(){
	    	pedidoAgregar(unProducto);
	    }

	    function showItem(){
	    	$item.delay(index*200).fadeIn(500);
	    }
	};*/

	} //Cierro objeto Listado

})(jQuery,PROYECTO,productos,undefined);