(function($,app,window){
  'use strict'

  app.Pedido ={

    init: function() {
      this.items = [];
    },

    pedidoAgregar : function(item){
      var indice = this.items.indexOf(item);
      var existe = indice >= 0;
      if(existe){
        this.items[indice].cantidad++;
      } else {
        item.cantidad = 1;
        this.items.push(item); 
      }
      app.Carrito.carritoDibujarPedido(); //Visualizo los items en el carrito
    },

    pedidoRemover : function(id){
      var size = this.items.length;
      for(var i=0; i < size; i++){
            var mismoId = this.items[i].id === id;
            if(mismoId){
              this.items[i].cantidad--;
              if(this.items[i].cantidad == 0){
                this.items.splice(i, 1);  
              }
              app.Carrito.carritoDibujarPedido(); //Visualizo los items en el carrito
              return;
            }
          }
          throw new Error('Item no encontrado');
    },

    pedidoVer :  function(){
      return this.items;
    },
    pedidoTotal : function(){
      var total = 0;
      this.items.forEach(function(item){ total+=item.precio*item.cantidad });
      return total;
    },
    pedidoEsVacio : function(){
      return this.items.length == 0;
    }

  };

  //////////////////////////

  /*pedidoInit = function(){
    items = [];
  };

  pedidoAgregar = function(item){
    var indice = items.indexOf(item);
    var existe = indice >= 0;
    if(existe){
      items[indice].cantidad++;
    } else {
      item.cantidad = 1;
      items.push(item); 
    }
    carritoDibujarPedido(); //Visualizo los items en el carrito
  };

  pedidoRemover = function(id){
    for(var i=0; i < items.length; i++){
      var mismoId = items[i].id === id;
      if(mismoId){
        items[i].cantidad--;
        if(items[i].cantidad == 0){
          items.splice(i, 1);  
        }
        carritoDibujarPedido(); //Visualizo los items en el carrito
        return;
      }
    }
    throw new Error('Item no encontrado');
  };

  pedidoVer =  function(){
    return items;
  },
  pedidoTotal = function(){
    var total = 0;
    items.forEach(function(item){ total+=item.precio*item.cantidad });
    return total;
  },
  pedidoEsVacio = function(){
    return items.length == 0;
  }*/

})(jQuery,PROYECTO,undefined);