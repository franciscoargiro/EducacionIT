(function($,app,window){

	app.Carrito = {
		init : function(){
			//Las variables $xxx representan objetos jQuery, es solo una nomenclatura
		    $container = $('.cart');
		    $boton = $('.cart__button', $container);
		    $listado = $('.cart__list', $container);
		    $total = $('.cart__total', $container);
		    template = $('#cart__item__template').html(); //En este caso template no lleva $ porque es texto
		    
		    $boton.click(this.carritoMostrarOcultar);
		},

		carritoMostrarOcultar :  function (){
    	$('.cart').toggleClass('cart--hidden');
  		},

	    carritoDibujarPedido : function(){
		    $listado.empty(); //Elimino todo lo que este dibujado en el listado previamente
		    app.Pedido.pedidoVer().forEach(this.carritoDibujarProducto, this); //Por cada item del pedido dibujo un producto en el carrito
		    $total.text(app.Pedido.pedidoTotal());//Actualizo el total
		},

		carritoDibujarProducto : function(unProducto){
		    var $item = $(template);
		    $('.cart__item__nombre', $item).html(unProducto.titulo);
		    $('.cart__item__cantidad', $item).html(unProducto.cantidad);
		    $('.cart__item__precio', $item).html(unProducto.precio);
		    $('.cart__item__remove', $item).click(removerProducto);
		    $listado.append($item);
		    
		    function removerProducto(){
		      app.Pedido.pedidoRemover(unProducto.id);
		    }

		}
	}


	//////////////////////////////////
 
  	/*carritoInit = function(){
  	  //Las variables $xxx representan objetos jQuery, es solo una nomenclatura
	    $container = $('.cart');
	    $boton = $('.cart__button', $container);
	    $listado = $('.cart__list', $container);
	    $total = $('.cart__total', $container);
	    template = $('#cart__item__template').html(); //En este caso template no lleva $ porque es texto
	    
	    $boton.click(carritoMostrarOcultar);
  };
  
  	carritoMostrarOcultar =  function (){
    	$('.cart').toggleClass('cart--hidden');
  };

    carritoDibujarPedido = function(){
	    $listado.empty(); //Elimino todo lo que este dibujado en el listado previamente
	    pedidoVer().forEach(carritoDibujarProducto, this); //Por cada item del pedido dibujo un producto en el carrito
	    $total.text(pedidoTotal());//Actualizo el total
	},

	carritoDibujarProducto = function(unProducto){
	    var $item = $(template);
	    $('.cart__item__nombre', $item).html(unProducto.titulo);
	    $('.cart__item__cantidad', $item).html(unProducto.cantidad);
	    $('.cart__item__precio', $item).html(unProducto.precio);
	    $('.cart__item__remove', $item).click(removerProducto);
	    $listado.append($item);
	    
	    function removerProducto(){
	      pedidoRemover(unProducto.id);
	    }
  }*/

})(jQuery,PROYECTO,undefined);