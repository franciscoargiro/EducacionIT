//"use strict";
a = 20;
//var a = 20;
console.log(a);


(function(){
"use strict";
var b = 30;
console.log(b);


}());



// -- Buenas prácticas: optimizar loops --

var listado = ['lunes', 'martes', 'miercoles'];

for( var i=0, total=listado.length; i < total; i++){
var item = listado[i];
console.log(item);
}


// -- Buenas Prácticas: mapeo de objetos --

function obtenerApellido(nombre){
	// creo un objeto JSON
	var apellidos = {
		francisco : "Argiro",
		gonzalo : "Bueno",
		mosquito : "Pablos",
		
	}


	// return nombre + " " + apellidos[nombre.toLowerCase()] || "Desconocido";
	var apellido = apellidos[nombre.toLowerCase()] || "Desconocido";
	return nombre + " " + apellido;
}


console.log(obtenerApellido("Juan"));



