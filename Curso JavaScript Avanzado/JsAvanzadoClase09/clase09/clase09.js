//Ejemplo Promesas

(function(){

	$(document).ready(init);

	function init(){
		var urlBusqueda = "https://api.mercadolibre.com/sites/MLA/search?q=neymar";
		var urlDetalle = "https://api.mercadolibre.com/items/";

		//Encadenamiento de promesas
		$.get(urlBusqueda)
		.then(guardarDatos)
		.then(obtenerPrimerDato)
		.then(obtenerDetalle)
		.then(dibujarHtml)
		.fail(mostrarError);

		//Callbacks
		function guardarDatos(meli_data){
			//Guardo en localStorage el resultado de la busqueda
			localStorage.setItem('meli_data',JSON.stringify(meli_data));
		}

		function obtenerPrimerDato(){
			var datos = JSON.parse(localStorage.getItem('meli_data'));
			return datos.results[0];
		}

		function obtenerDetalle(item){
			return $.get(urlDetalle+item.id+'/descriptions/');
		}

		function dibujarHtml(detalle){
			var desc;

			/*if(detalle[0].text === "") {
				desc = detalle[0].plain_text;
			}else{
				desc = detalle[0].text;
			}*/

			//Version simplificada del condicional	
			desc = detalle[0].text === "" ? detalle[0].plain_text : detalle[0].text;

			$('#contenido').append(desc);

			//Borrar contenido de localStorage
			localStorage.removeItem('meli_data');
		}

		function mostrarError(error){
			$('#contenido').append('<h1 style="color:red">Error en datos!</h1');
			console.log(error);
		}
	}

})();