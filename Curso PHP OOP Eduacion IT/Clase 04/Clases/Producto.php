<?php

	class Producto{

		private $cod_producto;
		private $nombre;
		private $precio;
		private $cantidad;

		private $pdo;
				

		public function getProducto(){
			return $this->cod_producto;
		}

		public function getNombre(){
			return $this->nombre;
		}

		public function getPrecio(){
			return $this->precio;
		}

		public function getCantidad(){
			return $this->cantidad;
		}

		// ------------

		public function setProducto($cod){
			$this->cod_producto=$cod;
		}

		public function setNombre($cod){
			$this->nombre=$cod;
		}

		public function setPrecio($cod){
			$this->precio=$cod;
		}

		public function setCantidad($cod){
			$this->cantidad=$cod;
		}
		

		public function __construct($cod,$pdo){
			$this->pdo = $pdo;
			$stmt =$this->pdo->prepare("SELECT * FROM productos WHERE cod_producto=$cod");
			$stmt->execute();

			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			// Lo cargo en las propiedades
			$this->codigo=$result['cod_producto'];
			$this->nombre=$result['nombre'];
			$this->precio=$result['precio'];
			$this->cantidad=1;

			// En este caso como ya no vamos a acceder mas a la tabla, cierro la conexión
			$this->pdo=null;	

		}


	}

?>












