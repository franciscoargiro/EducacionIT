<?php

 

class Curso

{

        private $materia;

        //private $alumnos;

        protected $alumnos;

        //public $alumnos;

        const CANTIDAD_MAXIMA = 30;

        

        public function __construct()

        {

              $this->alumnos=0;

              $this->materia="";

        }

        

        public function cargaCurso($m, $a)

        {

              if ( self::CANTIDAD_MAXIMA < $a )

                     echo ("no se puede superar la cantidad maxima.<br>");

              else

              {

                     $this->materia = $m;

                     $this->alumnos = $a;                        

              }

        }

        

        public function verCurso()

        {

              echo "Materia: <b>$this->materia</b> ($this->alumnos)";

        }

}

 

$x = new Curso;

$x->cargaCurso("Matematica", 15);

$x->verCurso();

echo "<hr>";

$x->cargaCurso("Lengua", 45);

$x->verCurso();

echo "<br>";
echo $x->alumnos;

?>