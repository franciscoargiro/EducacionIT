<?php


// Creamos la conexión
try{
	$objConexion = new PDO('mysql:host=localhost;dbname=dbprueba','root','fragiro789987');
	echo "Conectado a la Base de Datos"."<br><br>";
	} catch(PDOException $e) {
		echo $e->getMessage();
	}


/*
// Realizamos una consulta del tipo Prepare
$sql= 'SELECT * FROM personas';
$conn->prepare($sql);
echo
*/

// creo la consulta con PDO
$sql = $objConexion->prepare('SELECT * FROM personas');

// ejecuto la consulta con un metodo predefinido de PDO
$sql->execute();

// guardo la consulta en una variable con el mnetodo fechAll q crea un arreglo
$result= $sql->fetchAll();


// recorremos el arreglo con foreach y lo mostramos con un echo
foreach ($result as $key => $value) {
	echo $value['Nombre']."<br>";
	echo $value['Genero']."<br>";
} 