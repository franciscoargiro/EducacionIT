<?php

/**
 * @author educacionit
 * @copyright 2008
 */

include("Pelicula.php");

if(!empty($_GET['reserva']))
{
	// reservo la butaca
	$x=new Pelicula($_GET['reserva']);
	
	if($x->reservar()==false)	
	{
		echo "no se pudo reservar";
	}
	
}
else if(!empty($_GET['devuelve']))
{
	// devuelvo la butaca
	$x=new Pelicula($_GET['devuelve']);
	
	if($x->devolver()==false)	
	{
		echo "no se pudo devolver";
	}	

}


$hostname 	=	'localhost';
$username 	= 	'root';
$password	= 	'fragiro789987';
$dbname 	= 	'phpdb';
// abro la conexion
$pdo = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);



// preparo la query para obtener los codigos de todas las peliculas
$stmt = $pdo->query("SELECT cod_pelicula FROM peliculas");

?>
<table width=60% align="left" cellpadding="0" cellspacing="0" border=2>
<tr>
	<td>Codigo</td>
	<td>Titulo</td>
	<td>Genero</td>
	<td>Butacas</td>
	<td>Disponible</td>
	<td>Reservar</td>
	<td>Devolver</td>
</tr>


<?php
//traigo uno por uno los registros de la query, que en este caso solo
// contienen un campo (cod_pelicula). Luego intancio un objeto de clase Pelicula y muestro
//su registro en la tabla 
while($campos=$stmt->fetch(PDO::FETCH_ASSOC))
{
	$pelicula=new Pelicula($campos['cod_pelicula']);
 ?>

	<tr>
		<td><?=$pelicula->codigo?></td>
		<td><?=$pelicula->titulo?></td>
		<td><?=$pelicula->genero?></td>
		<td><?=$pelicula->butacas?></td>
		<td><?=$pelicula->disponibles?></td>
		
		<td><a href="reporte.php?reserva=<?=$pelicula->codigo?>">Reservar</a></td>
		<td><a href="reporte.php?devuelve=<?=$pelicula->codigo?>">Devolver</a></td>		
		
	</tr>
	
<?php } ?>	
	
</table>