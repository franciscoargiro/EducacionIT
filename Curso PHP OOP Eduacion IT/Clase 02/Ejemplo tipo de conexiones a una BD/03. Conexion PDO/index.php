<?php

require "Class/DataBase.php";


// Coneccion con PDO
$objDataBase = new DataBase();


// creo la consulta con PDO
$sql = $objDataBase->prepare('SELECT * FROM personas');

// ejecuto la consulta con un metodo predefinido de PDO
$sql->execute();

// guardo la consulta en una variable con el mnetodo fechAll q crea un arreglo
$result= $sql->fetchAll();


// recorremos el arreglo con foreach y lo mostramos con un echo
foreach ($result as $key => $value) {
	echo $value['Nombre']."<br>";
}

