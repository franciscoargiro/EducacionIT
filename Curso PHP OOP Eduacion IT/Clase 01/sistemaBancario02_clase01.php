<h1>SISTEMA BANCARIO 02 - LABORATORIO CLASE 01 EDUCACION IT</h1>
<br>
<br>

<?php


// --------------- CLASE CUENTA CORRIENTE Y CAJA AHORRO -------------------

abstract class Cuenta
{
	//public $titular_cuenta;
	public $saldo=0;


	public function verDatos(){
		//echo "Titular: ".$this->titular_cuenta;
		//echo "<br>";
		echo "Saldo: $".$this->saldo;
		echo "<br>";
	}
  
	
	public function depositar($deposito){
		$this->saldo=$this->saldo+$deposito;
		echo "<br>";
		echo "Deposito realizado, su nuevo saldo es: $".$this->saldo;
		echo "<br>";
	}


	public function extraer($extraccion) {
		$this->saldo=$this->saldo-$extraccion;
	}
  

		
}


			/**
			* 
			*/
			class CtaCte extends Cuenta
			{
				public $limite=-1000;

				public function verDatos(){
					echo "<br>";
					echo "CUENTA CORRIENTE";
					echo "<br>";
					echo "Limite Descubierto: $".$this->limite;
					echo "<br>";
					Cuenta::verDatos();

				}
			
				public function extraer($extraccion){
					if (($this->saldo-$extraccion)>$this->limite){
						parent::extraer($extraccion);
						echo "<br>";
						echo "Extraccion completa, el nuevo saldo de su CtaCte es: $".$this->saldo;
						echo "<br>";
					} else {
						echo "<br>";
						echo "Su limite no le permite realizar esta extraccion";
						echo "<br>";
					}
				}

			}


			/**
			* 
			*/
			class CajaAhorro extends Cuenta
			{
				
				public $interes;

				public function verDatos(){
					echo "<br>";
					echo "CAJA DE AHORROS";
					echo "<br>";					
					echo "Interes: ".$this->interes;
					echo "<br>";
					Cuenta::verDatos();
				}


				public function extraer($extraccion){
					if (($this->saldo-$extraccion)>=$this->saldo){
						parent::extraer($extraccion);
						echo "<br>";
						echo "Extraccion completa, el nuevo saldo de su Caja de Ahorro es: $".$this->saldo;
						echo "<br>";
					} else {
						echo "<br>";
						echo "Su limite no le permite realizar esta extraccion";
						echo "<br>";
					}
				}

			}



// --------------- CLASE CLIENTE TIPO EMPRESA Y PERSONA -------------------

/**
* 
*/
abstract class Cliente 
{
	public $nombre;
	public $tel;
	public $direccion;
	public $mail;
	
	public $ctaCte;  // var para instanciar la Cuenta Corriente dentro de cliente
 	public $cajaAhorro; // var para instanciar la Caja de Ahorro dentro de Cliente


 	// instancio las clases CtaCte y CajaAhorro dentro de la clase Cliente
 	public function crearCuentas() {
 		$this->ctaCte= new CtaCte();
 		$this->cajaAhorro= new CajaAhorro();
 	}

 	public function crearCliente(){
	 	$this->nombre = $nom;
		$this->tel= $te;
		$this->direccion= $dir;
		$this->mail= $m;
	}


	public function mostrarIdentidad(){
		echo "Nombre: ".$this->nombre;
		echo "<br>";
	}
		
	public function mostrarContacto(){
		echo "<br>";
		echo "CONTACTO: ";
		echo "<br>";
		echo "Telefono: ".$this->tel;
		echo "<br>";
		echo "Direccion: ".$this->direccion;
		echo "<br>";
		echo "Mail: ".$this->mail;
		echo "<br>";
	}	
	
	
}


			/**
			* 
			*/
			class Empresa extends Cliente
			{
				public $tipo;
				public $cuit;
				public $titular;
				

				function __construct($nom,$tip,$te,$dir,$m,$titu,$cu)
				{
					
					parent::crearCuentas();
					
					//parent::crearCliente();
					
					$this->nombre = $nom;
					$this->tel= $te;
					$this->direccion= $dir;
					$this->mail= $m;
					
					$this->tipo= $tip;
					$this->cuit = $cu;
					$this->titular= $titu;


				}

				public function mostrarIdentidad(){
					parent::mostrarIdentidad();
					echo " ".$this->tipo;
					echo "<br>";					
					echo "CUIT: ".$this->cuit;
					echo "<br>";
					echo "Titular: ".$this->titular;
					echo "<br>";
				}
			}


			/**
			* 
			*/
			class Persona extends Cliente
			{
				public $apellido;
				public $dni;
				public $estado_civil;

				
				function __construct($nom,$ape,$te,$dir,$m,$dn,$est)
				{
					
					parent::crearCuentas();
					
					//parent::crearCliente();
					
					$this->nombre = $nom;
					$this->tel= $te;
					$this->direccion= $dir;
					$this->mail= $m;
					

					$this->apellido=$ape;
					$this->dni=$dn;
					$this->estado_civil=$est;
				}


				public function mostrarIdentidad(){
					parent::mostrarIdentidad();
					echo "Apellido: ".$this->apellido;
					echo "<br>";
					echo "DNI: ".$this->dni;
					echo "<br>";
					echo "Estado Civil: ".$this->estado_civil;
					echo "<br>";
				}


			}







// METODOS DE LOS OBJETOS:

/*
	COMANDOS CLIENTES (Empresa / Persona)
		new Persona($nom,$ape,$te,$dir,$m,$dn,$est);
		new Empresa($nom,$tip,$te,$dir,$m,$titu,$cu);
		mostrarIdentidad();
		mostrarContacto();

	COMANDOS CUENTAS (CtaCte / CajaAhorro) tener en cuenta que siempre hay que definir el tipo de clase ctaCte o cajaAhorro
		ctaCte->verDatos();
		cajaAhorro->verDatos();
			->depositar($monto);
			->extraer($extraccion)
*/			



// INSTANCIAR OBJETOS:

	$fran= new Persona("Francisco","Argi",471354,"Saenz 1250","franarg@gmail.com",30300826,"soltero");
	$fran->mostrarIdentidad();
	$fran->mostrarContacto();
	
	$fran->cajaAhorro->depositar(200);
	$fran->cajaAhorro->verDatos();

	$fran->ctaCte->verDatos();

	$fran->ctaCte->depositar(100);	
	$fran->ctaCte->depositar(100);
	$fran->ctaCte->extraer(500);
    $fran->ctaCte->depositar(600);
    $fran->ctaCte->extraer(500);
    $fran->ctaCte->extraer(600);
    $fran->ctaCte->extraer(600);
	







// Prueba de que php corre bien
echo "<br>";
echo "<br>";
echo "PHP corre correctamente";
echo "<br>";

?>			