#!/ust/bin/env python
# -*- coding: utf-8 -*-

import Tkinter as tk

ventana_principal = tk.Tk()
ventana_principal.config(width=600, height= 300)

frame = tk.Frame(ventana_principal)
frame.place(x=0, y=0, width=400, height=300)

button = tk.Button(frame, text="Hola Mundo")
button.place(x=50, y=70, width=100, height=50)

ventana_principal.mainloop()