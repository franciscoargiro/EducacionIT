print("Hola Mundo")


a = 3.00/2.30
print(a)

b = 8**2  # Esto es elevado al cuadrado
print(b)

a = ["Francisco", "Mariano", "Cecilia", "Daniela"]
b = [3.14, True, False, "Hola Mundo"]

print(a[0])
print(b[1])

print(type(a)) # Las LISTAS son objetos mutables

a.append("Dario")
print(a)

a.insert(2,"Pedro") # Con insert puedo elegir donde agregar un valor en la lista
print(a)

del(a[2])
print(a)


print(len(a))



# Slicing: acceder a un intervalo en particular en una LISTA o TUPLA
print(a[2:4]) # la posicion 4 en este caso no se imprime, es exluyente el ultimo valor

print(a[1:]) # imprime a partir de la posicion 1 hasta el final

print(a[0:5:2]) # el tercer elemento son los intervalos (step)

